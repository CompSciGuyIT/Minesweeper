#include<stdio.h>
#include<string.h>      // strlen
#include<sys/socket.h>
#include<arpa/inet.h>   // inet_addr
#include<unistd.h>      // write 
#include <stdlib.h>
#include <stdbool.h>
#include <omp.h>        // omp_get_wtime -fopenmp

#define RANDOM_NUMBER_SEED 42
#define NUM_TILES_X 9
#define NUM_TILES_Y 9
#define NUM_MINES 1
#define BUFFER 1024
#define ROW 0
#define COLUMN 1

/**********************************************/
/*                 Stuctures                  */
/**********************************************/

/* Individual tile structure */
typedef struct {
    int board_column;
    int board_row;
    int adjacent_mines;
    bool unknown;
    bool has_flag;
    bool has_mine;
} Tile;

/* Structure to hold information regarding the state of the game */
typedef struct game_state game_t;
struct game_state {
    int mines_remaining;
    Tile game_board[NUM_TILES_Y][NUM_TILES_X];
};

/* Player information structure */
typedef struct game_player player_t;
struct game_player {
    char *name;
    int game_time;
    int games_won;
    int games_played;
    bool has_won;
};

/* Linked list generated from file */
typedef struct client client_t;
struct client {
    char *user;
    char *pass;
    client_t *next;
};

/* Linked list of leaderboard */
typedef struct leaderboard board_t;
struct leaderboard {
    char *name;
    int game_time;
    int games_won;
    int games_played;
    board_t *next;
};

/**********************************************/
/*           Function declarations            */
/**********************************************/

void initialise_gamestate(game_t *);
void generate_mines(game_t *);
bool check_and_assign_mine(game_t *, int, int);
void initialise_adjacent_mines_field(game_t *);
void print_game_board(game_t *);
client_t * parse_authentication_file(client_t *);
client_t * add_credentials(client_t *, client_t *);

/************* Remove prior to production!!! ***********************/
void node_print(client_t *);

char main_menu();
void read_user_input(char *, int);
bool main_option_valid(char *);
bool game_option_valid(char *);
bool tile_option_valid(char *);
void play_game(player_t *);
void show_leaderboard();
bool quit();
bool quit_game();
char game_menu();
bool reveal_tile(game_t *, player_t *);
bool check_tile_status(game_t *, player_t *, int *);
int convert_to_indices(char);
bool check_range(int *);
bool place_flag(game_t *, player_t *, int);
bool check_for_mine(game_t *, player_t *, int *, int);
void display_menu(player_t *);
player_t * sign_in_user();
player_t * authenticate_user(client_t *, player_t *, char *, char *);
void RemoveSpaces(char *);
void update_leaderboard();
void swap(board_t *, board_t *);
void sort_leaderboard(board_t *);
board_t * add_to_leaderboard(board_t *);
void pressEnter();
void print_scores(board_t *);
game_t * loss_screen(game_t *);

/**********************************************/
/*              Global variables              */
/**********************************************/

/* Server side variables */
int port_number = 12345;
board_t *scoreboard = NULL;
char ch[1];     // For 'Press ENTER key to Continue' functionality

/**********************************************/
/*              The Main Program              */
/**********************************************/

int main(int argc, char **argv) {
    client_t *creds_list = NULL;
    player_t *player = NULL;

    /* Parse user credentials from file to list */
    creds_list = parse_authentication_file(creds_list);    

    /* Seed the random number generator */
    srand(RANDOM_NUMBER_SEED);

    /* Initial clearing of the screen */
    system("clear");

    /* User sign in */
    player = sign_in_user(creds_list, player);

    /* Start game */
    while (player != NULL) {

        /* Display the main menu */
        display_menu(player);

        /* When player exits game */ 
        /* this will prevent the user being asked to sign in again */
        break;
    }

    /* Final clearing of the screen */
    system("clear");

    return 0;
}

/**********************************************/
/*              Program Functions             */
/**********************************************/

/* Print nodes in leaderboard */
void print_scores(board_t *node) {
    for ( ; node != NULL; node = node->next) {
        
        // %-10s the "-" justifies the strings to the left
        printf(" %-10s %10d seconds %10d games won, %2d games played\n", node->name, node->game_time, node->games_won, node->games_played);
    }
}

/* Press ENTER key to Continue */
void pressEnter() {
    printf("Press ENTER key to Continue\n");
    read_user_input(ch, 1); 

    return;
}

/* Swapping data of two nodes*/
void swap(board_t *first, board_t *second) 
{ 
    char *temp_name;
    int temp_game_time;
    int temp_games_played;
    int temp_games_won;

    temp_name = first->name; 
    temp_game_time = first->game_time;
    temp_games_played = first->games_played;
    temp_games_won = first->games_won;

    first->name = second->name; 
    first->game_time = second->game_time;
    first->games_played = second->games_played;
    first->games_won = second->games_won;

    second->name = temp_name; 
    second->game_time = temp_game_time;
    second->games_played = temp_games_played;
    second->games_won = temp_games_won;

    return;
} 

/* Bubble sort the given linked list */
void sort_leaderboard(board_t *head) 
{ 
    int swapped, i; 
    board_t *ptr; 
    board_t *lptr = NULL; 
  
    /* Checking for empty list */
    if (head == NULL) 
        return; 
  
    do
    { 
        swapped = 0; 
        ptr = head; 
  
        while (ptr->next != lptr) 
        {
            if ((ptr->game_time == ptr->next->game_time &&
                ptr->games_won == ptr->next->games_won &&
                ptr->name < ptr->next->name) ||

                (ptr->game_time == ptr->next->game_time &&
                ptr->games_won > ptr->next->games_won) ||

                (ptr->game_time < ptr->next->game_time))
            {  
                swap(ptr, ptr->next); 
                swapped = 1; 
            } 
            ptr = ptr->next; 
        } 
        lptr = ptr; 
    } 
    while (swapped);

    return;
}

/* Update the leaderboard */
void update_leaderboard(player_t *player) {
    
    board_t *head = scoreboard;

    /* If player already in leaderboard, update games_played */
    for ( ; scoreboard != NULL; scoreboard = scoreboard->next) {
        if (strcmp(scoreboard->name, player->name) == 0) {
            scoreboard->games_played = player->games_played;
            scoreboard->games_won = player->games_won;
        }
    }

    /* Reset scoreboard head */
    scoreboard = head;

    if (player->has_won) {

        /* Initialise data for new node */
        board_t new;

        new.name = strdup(player->name);
        new.game_time = player->game_time;
        new.games_played = player->games_played;
        new.games_won = player->games_won;
        
        /* Add new node to leaderboard */
        board_t *new_node = add_to_leaderboard(&new);
        if (new_node == NULL) {
            printf("memory allocation failure\n");

            exit(EXIT_FAILURE);
        }

        scoreboard = new_node;
    }

    return;
}

/* Add new ranking to the leaderboard */
board_t * add_to_leaderboard(board_t *head) {

    /* create new nose to add to the list */
    board_t *new = (board_t *)malloc(sizeof(board_t));
    if (new == NULL) {
        return NULL;
    }

    // insert new node 
    new->name = strdup(head->name);
    new->game_time = head->game_time;
    new->games_played = head->games_played;
    new->games_won = head->games_won;
    new->next = scoreboard;

    return new;
}

/* User authentication */
player_t * authenticate_user(client_t *creds_list, player_t *player, char *username, char *password) {
    for ( ; creds_list != NULL; creds_list = creds_list->next) {
        if ((strcmp(username, creds_list->user) == 0) && (strcmp(password, creds_list->pass) == 0)) {
            
            /* Initialise Player struct on successful log in */
            player = (player_t *) malloc (sizeof(player_t));
            strcpy(player->name, username);
            player->games_played = 0;
            player->games_won = 0;
            player->game_time = 0;

            return player;
        }
    }

    printf("\n\nYou entered either an incorrect username or password.  Disconnecting.\n");
    sleep(2);

    return NULL;
}


/* User sign in */
player_t * sign_in_user(client_t *creds_list, player_t *player) {

    char username[10];
    char password[10];

    printf("\n\n===============================================\n");
    printf("Welcome to the online Minesweeper gaming system\n");
    printf("===============================================\n\n");
    printf("You are required to log on with your registered user name and password.\n\n");
    printf("Username: ");
    read_user_input(username, 10);
    printf("\nPassword: ");
    read_user_input(password, 10);

    return authenticate_user(creds_list, player, username, password);
}

/* Main Menu */
void display_menu(player_t *player) {
    bool quit_game = false;
    char main_choice;

    system("clear");

    /* Do while user still wants to play game */
    do {
        /* Initialise player's has_won status */
        player->has_won = false;

        /* Prompt user from Main menu */
        main_choice = main_menu();

        switch (main_choice) {
            case '1':
                play_game(player);
                break;

            case '2':
                show_leaderboard(scoreboard);
                break;

            case '3':
                quit_game = quit();
                break;
        
            default:
                break;
        }

    } while (!quit_game);

    return;
}

/* Play Minesweeper */
void play_game(player_t *player) {
    /* Set up game */
    bool game_on = true;
    char game_choice;
    int start_time = omp_get_wtime();

    /* Allocate memory for game struct */
    game_t *game = malloc(sizeof(game_t));

    initialise_gamestate(game);

    /* Do while game is in play */
    do {
        /* Update play screen */
        print_game_board(game);

        /* Prompt user fromo game menu */
        game_choice = game_menu();

        switch (game_choice) {
            case 'R':
            case 'r':
                game_on = reveal_tile(game, player);
                break;

            case 'P':
            case 'p':
                game_on = place_flag(game, player, start_time);
                break;

            case 'Q':
            case 'q':
                game_on = quit_game(player);
                break;
        
            default:
                break;
        }
        
    } while (game_on);
    
    return;
}

/* Check entered coordinates for mine */
bool check_for_mine(game_t *game, player_t *player, int *coords, int start_time) {

    /* Check if tile has already been revealed */
    if (!(game->game_board[coords[0]][coords[1]].unknown)) {

        return true;
    }

    /* Check if tile contains a mine */
    if (game->game_board[coords[0]][coords[1]].has_mine) {
        game->mines_remaining--;
        game->game_board[coords[0]][coords[1]].has_flag = true;
        game->game_board[coords[0]][coords[1]].unknown = false;

        /* Check if player has flagged all the mines */
        if (game->mines_remaining < 1) {

            /* Get game finish time */
            int end_time = omp_get_wtime();

            /* Increment the number of games played */
            player->games_played++;
    
            /* Increment the number of games won */
            player->games_won++;

            /* Adjust has_won variable */
            player->has_won = true;

            /* Calculate win time */
            player->game_time = end_time - start_time;

            /* Add to leaderboard */
            update_leaderboard(player);
            
            /* Update play screen */
            print_game_board(game);

            /* Display Win */
            printf("\nCongratulations!  You have located all the mines.\n");
            printf("\nYou won in %d seconds!\n\n", player->game_time);

            pressEnter();

            system("clear");

            return false;
        }
    } else {
        printf("\nNo mine at this location!\n\n");
        
        pressEnter();
    }

    return true;
}

/* place flag at designated coordinates if mine exists there */
bool place_flag(game_t *game, player_t *player, int start_time) {
    char coords[3];
    int indices[2];

    /* Prompt user for tile coordinates */
    printf("\nEnter tile coordinates: ");
    do {
        read_user_input(coords, 3);

    } while (!tile_option_valid(coords));

    /* Convert user input to array indices */
    indices[0] = convert_to_indices(coords[0]);
    indices[1] = convert_to_indices(coords[1]);

    /* Check tile for mine*/
    return check_for_mine(game, player, indices, start_time);
}

/* Convert the validated corrdinates to array indices */
int convert_to_indices(char coord) {
        switch (coord) {
            case 'A':
            case 'a':
            case '1':
                return 0;
                break;
        
            case 'B':
            case 'b':
            case '2':
                return 1;
                break;
        
            case 'C':
            case 'c':
            case '3':
                return 2;
                break;
        
            case 'D':
            case 'd':
            case '4':
                return 3;
                break;
        
            case 'E':
            case 'e':
            case '5':
                return 4;
                break;
        
            case 'F':
            case 'f':
            case '6':
                return 5;
                break;
        
            case 'G':
            case 'g':
            case '7':
                return 6;
                break;
        
            case 'H':
            case 'h':
            case '8':
                return 7;
                break;
        
            case 'I':
            case 'i':
            case '9':
                return 8;
                break;
        }
}

/* Check the coordinates are within the range of the gameboard */
bool check_range(int *indices) { 
    if ((indices[ROW] < 0) ||
        (indices[ROW] > 8) ||
        (indices[COLUMN] < 0) ||
        (indices[COLUMN] >8)) {
            return true;
        }
    return false;
}

/* Reveal all mine locations, hide everything else */
game_t * loss_screen(game_t *game) {

    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int column = 0; column < NUM_TILES_X; column++) {
            if (game->game_board[row][column].has_mine) {
                game->game_board[row][column].unknown = false;
            } else {
                game->game_board[row][column].unknown = true;
            }
        }
    }

    return game;
}

/* Check the user's input against the tiles on the gameboard */
bool check_tile_status(game_t *game, player_t *player, int *coords) {
    /* This is a recursive function */
    /* Check for base cases */

    /* Check for mine */
    if (game->game_board[coords[0]][coords[1]].has_mine) {

        /* Reveal all mine locations, hide everything else */
        game = loss_screen(game);

        /* Increment the number of games played */
        player->games_played++;

        /* Update leaderboard if player exists on it */
        update_leaderboard(player);
            
        /* Update play screen */
        print_game_board(game);
        
        /* Display Loss */
        printf("\nYOU DETONATED A MINE!!\n");
        printf("Bad luck.  You lose!\n\n");

        pressEnter();

        system("clear");

        return false;
    }

    /* Check if tile has already been revealed */
    if (!(game->game_board[coords[0]][coords[1]].unknown)) {

        return true;
    }

    /* Check if adjacent to mine */
    if (game->game_board[coords[0]][coords[1]].adjacent_mines > 0) {
        game->game_board[coords[0]][coords[1]].unknown = false;
        
        return true;
    }

    /* Reveal tile */
    game->game_board[coords[0]][coords[1]].unknown = false;
    
    int indices[2];

    /* Recursively check surrounding tiles */
    /* Get neighbouring tile coordinates */
    for (int neighbour = 0; neighbour < 8; neighbour++) {
        
        switch (neighbour) {
            case 0:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 1:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN];

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 2:
                indices[ROW] = coords[ROW] - 1;
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 3:
                indices[ROW] = coords[ROW];
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 4:
                indices[ROW] = coords[ROW];
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 5:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN] - 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 6:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN];

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            case 7:
                indices[ROW] = coords[ROW] + 1;
                indices[COLUMN] = coords[COLUMN] + 1;

                /* Check that tile is not out of bounds */
                if (check_range(indices)) {
                        break;
                }

                check_tile_status(game, player, indices);

                break;
        
            default:
                break;
        }
    }

    return true;
}

/* Validate user input of tile selection */
bool tile_option_valid(char *opt) {

    bool choice = false;

    if ((((opt[0] >= 'a') && (opt[0] <= 'i')) ||
        ((opt[0] >= 'A') && (opt[0] <= 'I'))) &&
        ((opt[1] >= '1') && (opt[1] <= '9'))) {
            choice = true;
        }

    if (!choice) {
        printf("\nInvalid option!\n");
        printf("\nThe first should be a character in the range [A - I]\n");
        printf("\nThe second should be a digit in the range [1 - 9]\n");
        printf("Enter tile coordinates: ");
    }

    return choice;
}

/* Reveal the user selected tile */
bool reveal_tile(game_t *game, player_t *player) {
    char coords[3];
    int indices[2];

    /* Prompt user for tile coordinates */
    printf("\nEnter tile coordinates: ");
    do {
        read_user_input(coords, 3);
    } while (!tile_option_valid(coords));

    /* Convert user input to array indices */
    indices[0] = convert_to_indices(coords[0]);
    indices[1] = convert_to_indices(coords[1]);

    /* Check the status of the tile */
    return check_tile_status(game, player, indices);
}

/* Quit game */
bool quit_game(player_t *player) {
    /* Increment the number of games played */
    player->games_played++;
    
    printf("\n\nNever mind.  Better luck next time!\n\n");

    pressEnter();

    system("clear");

    return false;
}

/* Game menu screen */
char game_menu() {
    char choice[2];

    printf("\n\nChoose an option:\n");
    printf("<R>	Reveal tile\n");
    printf("<P>	Place flag\n");
    printf("<Q>	Quit game\n\n");
    printf("Option (R, P, Q): ");

    do {
        read_user_input(choice, 2);
    } while (!game_option_valid(choice));

    return choice[0];
}

/* Validate user input of game menu selection */
bool game_option_valid(char *opt) {
    bool choice = false;

    switch (*opt) {
        case 'R':
        case 'P':
        case 'Q':
        case 'r':
        case 'p':
        case 'q':
            choice = true;
    }

    if (!choice) {
        printf("\nInvalid option!\n");
        printf("Please Choose 'R', 'P', or 'Q'\n\n");
    }

    return choice;
}

/* Display leaderboard */
void show_leaderboard() {
    system("clear");

    if (scoreboard == NULL) {
        printf("\n\n==============================================================================\n\n");
        printf("There is no information currently stored in the leaderboard.  Try again later.\n\n");
        printf("==============================================================================\n\n");
        
        pressEnter();
    } else {
        printf("\n\n=====================================================================\n");
        printf("=====================  Minesweeper Leaderboard  =====================\n");
        printf("=====================================================================\n\n");

        /* Sort the leaderboard */
        sort_leaderboard(scoreboard);

        /* Print the sorted leaderboard */
        print_scores(scoreboard);

        printf("\n=====================================================================\n\n");
        
        pressEnter(); 
    }

    system("clear");

    return;
}

/* Exit program */
bool quit() {
    printf("\n\nThanks for playing Minesweeper!\n");
    printf("See you again soon.\n\n");

    sleep(2);

    system("clear");

    return true;
}

/* Main menu screen */
char main_menu() {
    char choice[2];

    printf("\n\n===============================================\n");
    printf("Welcome to the online Minesweeper gaming system\n");
    printf("===============================================\n\n");
    printf("Please enter a selection:\n");
    printf("<1>	Play Minesweeper\n");
    printf("<2>	Show Leaderboard\n");
    printf("<3>	Quit\n\n");
    printf("Selection option (1 – 3): ");

    do {
        read_user_input(choice, 2);
    } while (!main_option_valid(choice));

    return choice[0];
}

/* Validate user input of main menu selection */
bool main_option_valid(char *opt) {
    bool choice = false;

    switch (*opt) {
        case '1':
        case '2':
        case '3':
            choice = true;
    }

    if (!choice) {
        printf("\nInvalid option!\n");
        printf("Selection option (1 – 3): ");
    }

    return choice;
}

/* Reads user input and clears input buffer */
/* Character array must have one extra element */
/* to take into account the null terminator character */
void read_user_input(char s[], int maxlen) {

	char ch;
	int i;
	int chars_remain;
	i = 0;
	chars_remain = 1;

	while (chars_remain) {
		ch = getchar();
		if ((ch == '\n') || (ch == EOF) ) {
			chars_remain = 0;
		} else if (i < maxlen - 1) {
			s[i] = ch;
			i++;
		}
	}
	s[i] = '\0';

	return;
}

/* Create a node in the linked list of users from Authentication file */
client_t * add_credentials(client_t *creds_list, client_t *head) {

    // create new node to add to list
    client_t *new = (client_t *)malloc(sizeof(client_t));
    if (new == NULL) {
        return NULL;
    }

    // insert new node
    new->user = strdup(head->user);     // strdup duplicates the string by allocating memory for it
    new->pass = strdup(head->pass);     // this prevents copying over previous nodes
    new->next = creds_list;

    return new;
}

/************* Remove prior to production!!! ***********************/
void node_print(client_t *head) {
    for ( ; head != NULL; head = head->next) {
        printf("User=%s Password=%s\n", head->user, head->pass);
    }

    return;
}

/* Retrieves all user credentials from file and stores them in a list */
client_t * parse_authentication_file(client_t *creds_list) {
    FILE * fp;
    char buffer[20];

    fp = fopen("Authentication.txt", "r");

    /* skip first line of file */
    fscanf(fp, "%*[^\n]\n");

    while (fscanf(fp, "%[^\n]\n", buffer) != EOF) {
        int counter = 0;

        char *token = strtok(buffer, "\t"); 
        
        /* Create new user */
        client_t new_user;

        // Keep printing tokens while one of the 
        // delimiters present in str[]. 
        while (token != NULL) {
            
            switch (counter) {
                case 0:
                    RemoveSpaces(token);
                    new_user.user = token;
                    counter++;
                    break;
                case 1:
                    RemoveSpaces(token);
                    new_user.pass = token;
                    break;                
                default:
                    break;
            }

            token = strtok(NULL, "\t");
        }

        // Add user to linked list
        client_t *newhead = add_credentials(creds_list, &new_user);
        if (newhead == NULL) {
            printf("memory allocation failure\n");
            
            exit(EXIT_FAILURE);
        }
        creds_list = newhead;
    }

    fclose(fp);

    return creds_list;
}

/* Removes the spaces from strings */
void RemoveSpaces(char* string) {
    int count = 0;

    for (int index = 0; string[index]; index++) {
        if (string[index] != ' ')
            string[count++] = string[index];
    }

    string[count] = '\0';

    return;
}

/* Initialises the game state at the beginning of each game */
void initialise_gamestate(game_t *game) {

    /* Assign variables within struct */
    game->mines_remaining = NUM_MINES;

    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int column = 0; column < NUM_TILES_X; column++) {
            game->game_board[row][column].board_row = row;
            game->game_board[row][column].board_column = column;
            game->game_board[row][column].adjacent_mines = 0;
            game->game_board[row][column].unknown = true;
            game->game_board[row][column].has_flag = false;
            game->game_board[row][column].has_mine = false;
        }
    }

    /* Generate the mines and assign them to a tile */
    generate_mines(game);

    /* For all tiles not contailing a mine */
    /* find how many surrounding tiles contain mines */
    /* and assign the value to the tile */
    initialise_adjacent_mines_field(game);

    return;
}

/* Generates the location of each mine on the gameboard */
void generate_mines(game_t *game) {
    int mine_row, mine_col;

    for (int mines = 0; mines < NUM_MINES; mines++) {
        do {
            mine_row = rand() % NUM_TILES_Y;
            mine_col = rand() % NUM_TILES_X;
        } while (check_and_assign_mine(game, mine_row, mine_col));
    }

    return;
}

/* Checks to see if tile already has been assigned a mine */
/* If not, a mine will be assigned to it and return false */
/* If the tile already has a mine assigned to it, return true */
bool check_and_assign_mine(game_t *game, int mine_row, int mine_col) {
    if (game->game_board[mine_row][mine_col].has_mine)
        return true;

    game->game_board[mine_row][mine_col].has_mine = true;

    return false;
}

/* Initialises the adjacent_mines field by checking all adjacent tiles for mines */
void initialise_adjacent_mines_field(game_t *game) {
    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int column = 0; column < NUM_TILES_X; column++) {

            /* Disregard all tiles that have mines */
            if (game->game_board[row][column].has_mine) {
                continue;
            }

                /* Check tile to the above left */
            if (!(((row - 1) < 0) || ((column - 1) < 0)) &&             // Check if will be out of scope
                game->game_board[row - 1][column - 1].has_mine) {       // Check if has mine
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile above */
            if (!((row - 1) < 0) &&
                game->game_board[row - 1][column].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the above right */        
            if (!(((row - 1) < 0) || ((column + 1) >= NUM_TILES_X)) &&
                game->game_board[row - 1][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the left */           
            if (!((column - 1) < 0) &&
                game->game_board[row][column - 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the right */
            if (!((column + 1) >= NUM_TILES_X) &&
                game->game_board[row][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the below left */
            if (!(((row + 1) >= NUM_TILES_Y) || ((column - 1) < 0)) &&
                game->game_board[row + 1][column - 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile below */
            if (!((row + 1) >= NUM_TILES_Y) &&
                game->game_board[row + 1][column].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }

            /* Check tile to the below right */
            if (!(((row + 1) >= NUM_TILES_Y) || ((column + 1) >= NUM_TILES_X)) &&
                game->game_board[row + 1][column + 1].has_mine) {
                    game->game_board[row][column].adjacent_mines++;
            }      
        }
    }

    return;
}

void print_game_board(game_t *game) {

    char tile[NUM_TILES_Y * NUM_TILES_X];
    int counter = 0;

    system("clear");
    
    for (int row = 0; row < NUM_TILES_Y; row++) {
        for (int col = 0; col < NUM_TILES_X; col++) {

            if (game->game_board[row][col].has_flag) {
                tile[counter] = '+';

            } else if (!(game->game_board[row][col].unknown)) {

                /* In cases where the player loses */
                if (game->game_board[row][col].has_mine) {
                    tile[counter] = '@';
                } else {                    
                    // adding '0' converts the integer to the corresponding character
                    tile[counter] = game->game_board[row][col].adjacent_mines + '0';
                }
            } else {
                tile[counter] = ' ';
            }
            counter++;            
        }
    }

    /* Display the game board */
    printf("\n\nRemaining mines: %d\n\n", game->mines_remaining);
    printf("        1   2   3   4   5   6   7   8   9\n");
    printf("============================================\n");
    printf("  A  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tile[0], tile[1], tile[2], tile[3], tile[4], tile[5], tile[6], tile[7], tile[8]);
    printf("     ||-----------------------------------||\n");
    printf("  B  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tile[9], tile[10], tile[11], tile[12], tile[13], tile[14], tile[15], tile[16], tile[17]);
    printf("     ||-----------------------------------||\n");
    printf("  C  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tile[18], tile[19], tile[20], tile[21], tile[22], tile[23], tile[24], tile[25], tile[26]);
    printf("     ||-----------------------------------||\n");
    printf("  D  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tile[27], tile[28], tile[29], tile[30], tile[31], tile[32], tile[33], tile[34], tile[35]);
    printf("     ||-----------------------------------||\n");
    printf("  E  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tile[36], tile[37], tile[38], tile[39], tile[40], tile[41], tile[42], tile[43], tile[44]);
    printf("     ||-----------------------------------||\n");
    printf("  F  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n", 
            tile[45], tile[46], tile[47], tile[48], tile[49], tile[50], tile[51], tile[52], tile[53]);
    printf("     ||-----------------------------------||\n");
    printf("  G  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tile[54], tile[55], tile[56], tile[57], tile[58], tile[59], tile[60], tile[61], tile[62]);
    printf("     ||-----------------------------------||\n");
    printf("  H  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tile[63], tile[64], tile[65], tile[66], tile[67], tile[68], tile[69], tile[70], tile[71]);
    printf("     ||-----------------------------------||\n");
    printf("  I  || %c | %c | %c | %c | %c | %c | %c | %c | %c ||\n",
            tile[72], tile[73], tile[74], tile[75], tile[76], tile[77], tile[78], tile[79], tile[80]);
    printf("============================================\n");

    return;
}