#pragma once

#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define BACKLOG 10
#define RETURNED_ERROR -1
#define RANDOM_NUMBER_SEED 42
#define NUM_TILES_X 9
#define NUM_TILES_Y 9
#define NUM_MINES 10
#define ROW 0
#define COLUMN 1
#define MAX_NAME_LENGTH 16
#define MAX_NUMBER_LENGTH 5
#define NUM_COORDS 2
#define MAXDATASIZE 100
#define RESULTS_SIZE 3
#define MAX_ARRAY_SIZE 20
#define QUIT 3

#include <stdbool.h>
#include <stdlib.h> 
#include <pthread.h>

/**********************************************/
/*                 Stuctures                  */
/**********************************************/

/* Individual tile structure */
typedef struct {
    int adjacent_mines;
    bool unknown;
    bool has_flag;
    bool has_mine;
} Tile;

/* Structure to hold information regarding the state of the game */
typedef struct game_state game_t;
struct game_state {
    int mines_remaining;
    Tile game_board[NUM_TILES_Y][NUM_TILES_X];
};

/* Player information structure */
typedef struct game_player player_t;
struct game_player {
    int newfd;
    char name[MAX_NAME_LENGTH];
    int game_time;
    int games_won;
    int games_played;
    bool has_won;
};

/* Linked list generated from file */
typedef struct client client_t;
struct client {
    char *user;
    char *pass;
    client_t *next;
};

/* Linked list of leaderboard */
typedef struct leaderboard board_t;
struct leaderboard {
    char *name;
    int game_time;
    int games_won;
    int games_played;
    board_t *next;
};

/* Linked list of requests */
typedef struct request request_t;
struct request {
    int number;
    request_t *next;
};

/* Linked list of players */
typedef struct players players_t;
struct players {
    player_t player;
    players_t *next;
    players_t *prev;
};

/* Linked list of games */
typedef struct games games_t;
struct games {
    game_t game;
    games_t *next;
};


/**********************************************/
/*              Global variables              */
/**********************************************/

extern board_t *scoreboard;
extern client_t *creds_list;
extern players_t *players_list;
extern games_t *games_list;
extern request_t *requests;
extern request_t *last_request;

extern pthread_mutex_t rand_mutex;
extern pthread_mutex_t read_count_mutex;
extern pthread_mutex_t rw_leader_mutex;
extern pthread_mutex_t players_mutex;
extern pthread_mutex_t games_mutex;
extern pthread_mutex_t creds_mutex;
extern pthread_mutex_t threads_mutex;
extern pthread_mutex_t request_mutex;
extern pthread_cond_t got_request;

extern int num_requests;
extern int new_fd;
extern int sockfd;
extern int read_count;

extern pthread_t p_threads[BACKLOG];
extern pthread_attr_t t_attr[BACKLOG];


/**********************************************/
/*           Function declarations            */
/**********************************************/

void initialise_gamestate(game_t *);
void place_mines(game_t *);
bool check_and_assign_mine(game_t *, int, int);
void initialise_adjacent_mines_field(game_t *);
void send_game_board(player_t *, game_t *, bool);
client_t * parse_authentication_file(client_t *);
void play_game(player_t *);
bool quit(int);
bool end_game(player_t *);
bool place_flag(game_t *, player_t *, int);
bool check_for_mine(game_t *, player_t *, int *, int);
bool reveal_tile(game_t *, player_t *);
bool check_tile_status(game_t *, player_t *, int *);
bool check_range(int *);
player_t * authenticate_user(client_t *, player_t *, char *, char *, int);
void RemoveSpaces(char *);
void run_game(player_t *);
board_t * add_to_leaderboard(board_t *);
void remove_from_leaderboard(player_t *);
int count_nodes();
void send_leaderboard(int);
void sort_leaderboard(board_t *);
void show_leaderboard();
void swap(board_t *, board_t *);
void update_leaderboard();
void prompt_client(int);
void receive_prompt(int);
void freeCredsList(client_t *);
void freeScoreboard(board_t *);
void freeRequestList(request_t *);
void freeGamesList(games_t *);
void freePlayersList(players_t *);
void sigHandler(int);
request_t * get_request(pthread_mutex_t *);
void add_request(int, pthread_mutex_t *, pthread_cond_t *);
void handle_request(request_t *);
void * handle_requests_loop();
void graceful_shutdown();
players_t * add_player(player_t *);
void remove_from_players_list(player_t *);
games_t * add_game(games_t *, game_t *);
void close_sockets(players_t *);
void cancel_threads();
void destroy_mutexes();
player_t * check_leaderboard(player_t *);
