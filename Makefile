CC = gcc
FLAGS = -std=gnu99 -Wall -Werror -g
LIBS = -lpthread -fopenmp
TARGETS = server client
RM = /bin/rm -f

all: $(TARGETS)

clean:
	$(RM) $(TARGETS)
	- rm *.o

rebuild: clean all

server: server.o server_functions.o
	$(CC) $(FLAGS) -o $@ $^ $(LIBS)

server.o: server.c server_functions.c
	$(CC) $(FLAGS) -c $^

client: client.o client_functions.o
	$(CC) $(FLAGS) -o $@ $^

client.o: client.c client_functions.c
	$(CC) $(FLAGS) -c $^

.PHONY: clean
