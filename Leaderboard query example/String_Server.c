/*
*  Materials downloaded from the web. See relevant web sites listed on OLT
*  Collected and modified for teaching purpose only by Jinglan Zhang, Aug. 2006
*/


#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>

	#define MYPORT 54320    /* the port users will be connecting to */


	#define BACKLOG 10     /* how many pending connections queue will hold */

typedef struct leaderboard board_t;
struct leaderboard {
    char *name;
    int game_time;
    int games_won;
    int games_played;
    board_t *next;
};

board_t *scoreboard = NULL;

/* Add new ranking to the leaderboard */
board_t * add_to_leaderboard(board_t *head) {

    /* create new nose to add to the list */
    board_t *new = (board_t *)malloc(sizeof(board_t));
    if (new == NULL) {
        return NULL;
    }

    // insert new node 
    new->name = strdup(head->name);
    new->game_time = head->game_time;
    new->games_played = head->games_played;
    new->games_won = head->games_won;
    new->next = scoreboard;

    return new;
}

/* Print nodes in leaderboard */
void print_scores(board_t *node) {
    for ( ; node != NULL; node = node->next) {
        
        // %-10s the "-" justifies the strings to the left
        printf(" %-10s %10d seconds %10d games won, %2d games played\n", node->name, node->game_time, node->games_won, node->games_played);
    }
}

int count_nodes(int new_fd) {


    int count = 0;
    board_t *head = scoreboard;

    while (scoreboard != NULL) {
        count++;

        scoreboard = scoreboard->next;
    }

    scoreboard = head;

    /* Send number of nodes to client */ 
    uint16_t node_count = htons(count);
	send(new_fd, &node_count, sizeof(uint16_t), 0);


    return count;
}

void send_leaderboard(int new_fd) {


    int max_size = 20;
    board_t *head = scoreboard;


    /* Convert the linked list to an array */
    for ( ; scoreboard != NULL; scoreboard = scoreboard->next) {
        
        char *arr;
        arr = malloc(max_size);
        memset(arr, 0, max_size);

        char number1[5];
        char number2[5];
        char number3[5];
        char ch[2];
        int numbytes;

        strcpy(arr, scoreboard->name);
        strcat(arr, " ");
        sprintf(number1, "%d", scoreboard->games_played);
        
        strcat(arr, number1);
        strcat(arr, " ");
        sprintf(number2, "%d", scoreboard->games_won);
        
        strcat(arr, number2);
        strcat(arr, " ");
        sprintf(number3, "%d", scoreboard->game_time);
        
        strcat(arr, number3);
        strcat(arr, "\0");
        

        if (send(new_fd, arr, strlen(arr) + 1, 0) == -1)
			perror("send");
            

        if ((numbytes=recv(new_fd, &ch, 2, 0)) == -1) {
            perror("recv");
            exit(1);
        }
    }
    scoreboard = head;

    return;
}

int main(int argc, char *argv[])
{
	int sockfd, new_fd;  /* listen on sock_fd, new connection on new_fd */
	struct sockaddr_in my_addr;    /* my address information */
	struct sockaddr_in their_addr; /* connector's address information */
	socklen_t sin_size;

    char *names[] = {"Mark", "James", "Sally", "Suresh"};

    /* Create the linked list */
    for (int index = 0; index < 4; index++) {
        /* Initialise data for new node */
        board_t new;

        new.name = names[index];
        new.game_time = (index + 1) * 10;
        new.games_played = (index + 1) * 3;
        new.games_won = (index + 1) * 2;
        
        /* Add new node to leaderboard */
        board_t *new_node = add_to_leaderboard(&new);
        if (new_node == NULL) {
            printf("memory allocation failure\n");

            exit(EXIT_FAILURE);
        }

        scoreboard = new_node;

    }

    /* Print the linked list */
    print_scores(scoreboard);


	/* generate the socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	/* generate the end point */
	my_addr.sin_family = AF_INET;         /* host byte order */
	my_addr.sin_port = htons(MYPORT);     /* short, network byte order */
	my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */
		/* bzero(&(my_addr.sin_zero), 8);   ZJL*/     /* zero the rest of the struct */

	/* bind the socket to the end point */
	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
	== -1) {
		perror("bind");
		exit(1);
	}

	/* start listnening */
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	printf("server starts listnening ...\n");











	/* repeat: accept, send, close the connection */
	// /* for every accepted connection, use a sepetate process or thread to serve it */
	while(1) {  /* main accept() loop */
		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
		&sin_size)) == -1) {
			perror("accept");
			continue;
		}
		printf("server: got connection from %s\n", \
			inet_ntoa(their_addr.sin_addr));


        /* Count number of nodes in leaderboard */
        /* Send result to Client */
        int num_nodes = count_nodes(new_fd);

        /* Convert the linked list node to an array */
        /* And send to client if nodes > 0 */
        if (num_nodes) {
            
            send_leaderboard(new_fd);
        }















		close(new_fd);  /* parent doesn't need this */

		// while(waitpid(-1,NULL,WNOHANG) > 0); /* clean up child processes */
	}

}
